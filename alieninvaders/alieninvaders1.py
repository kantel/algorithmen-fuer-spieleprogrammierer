import pgzrun

WIDTH = 600
HEIGHT = 600
TITLE = "Alien Invaders 1"

player_ship = Actor("playership")
player_ship.centerx = WIDTH/2
player_ship.bottom = HEIGHT - 20
player_speed = 5

def update():
    if keyboard.left:
        player_ship.centerx -= player_speed
        if player_ship.left <= 0:
            player_ship.left = 0
    elif keyboard.right:
        player_ship.centerx += player_speed
        if player_ship.right >= WIDTH:
            player_ship.right = WIDTH

def draw():
    screen.blit(images.background, (0, 0))
    player_ship.draw()

pgzrun.go()