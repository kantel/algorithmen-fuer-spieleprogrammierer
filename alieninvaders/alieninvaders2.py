import pgzrun

WIDTH = 600
HEIGHT = 600
TITLE = "Alien Invaders 2"

player_ship = Actor("playership")
player_ship.centerx = WIDTH/2
player_ship.bottom = HEIGHT - 20
player_speed = 5

bullet = Actor("laserred16")
bullet_speed = 25
bullet_fire = False

def update():
    global bullet_fire
    if keyboard.left:
        player_ship.centerx -= player_speed
        if player_ship.left <= 0:
            player_ship.left = 0
    elif keyboard.right:
        player_ship.centerx += player_speed
        if player_ship.right >= WIDTH:
            player_ship.right = WIDTH
    if not bullet_fire:
        bullet.bottom = player_ship.bottom
        bullet.centerx = player_ship.centerx
    if keyboard.SPACE:
        bullet_fire = True
    if bullet_fire:
        bullet.bottom -= bullet_speed
        if bullet.bottom <= 0:
            bullet_fire = False

def draw():
    screen.blit(images.background, (0, 0))
    bullet.draw()
    player_ship.draw()

pgzrun.go()