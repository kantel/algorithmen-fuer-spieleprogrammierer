import pgzrun
from random import randint

WIDTH = 600
HEIGHT = 600
TITLE = "Alien Invaders 4"

player_ship = Actor("playership")
player_ship.centerx = WIDTH/2
player_ship.bottom = HEIGHT - 20
player_speed = 5

bullet = Actor("laserred16")
bullet_speed = 25
bullet_fire = False

def reset_enemy():
    return (randint(25, WIDTH - 25), randint(-200, -50))

num_enemies = 5
enemies = []
for _ in range(num_enemies):
    enemy = Actor("enemyred")
    enemy.pos = reset_enemy()
    enemies.append(enemy)
enemy_speed = 2

score = 0
game_over = False

def update():
    global bullet_fire, score, enemy_speed, bullet_speed, player_speed, game_over
    if keyboard.left:
        player_ship.centerx -= player_speed
        if player_ship.left <= 0:
            player_ship.left = 0
    elif keyboard.right:
        player_ship.centerx += player_speed
        if player_ship.right >= WIDTH:
            player_ship.right = WIDTH
    if not bullet_fire:
        bullet.bottom = player_ship.bottom
        bullet.centerx = player_ship.centerx
    if keyboard.SPACE:
        bullet_fire = True
    if bullet_fire:
        bullet.bottom -= bullet_speed
        if bullet.bottom <= 0:
            bullet_fire = False
    for enemy in enemies:
        enemy.top += enemy_speed
        bullet_hit = bullet.colliderect(enemy)
        if bullet_hit:
            enemy.pos = reset_enemy()
            bullet.bottom = player_ship.bottom
            bullet.centerx = player_ship.centerx
            bullet_fire = False
            score += 1
        enemy_hit = enemy.colliderect(player_ship)
        if enemy_hit:
            game_over = True
            enemy.pos = reset_enemy()
            player_speed = enemy_speed = bullet_speed = 0
        if enemy.top >= HEIGHT:
            score -= 1
            enemy.pos = reset_enemy()
            if score <= 0:
                player_speed = enemy_speed = bullet_speed = score = 0
                game_over = True

def draw():
    screen.blit(images.background, (0, 0))
    for enemy in enemies:
        enemy.draw()
    bullet.draw()
    player_ship.draw()
    screen.draw.text("Score: " + str(score), (10, 10))
    if game_over:
        screen.draw.textbox("Game Over!", (95, 200, 400, 80), color = "red")

pgzrun.go()