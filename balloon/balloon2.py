import pgzrun
from random import randint

WIDTH = 640
HEIGHT = 480
TITLE = "🎈 Ein roter Luftballon 🎈"

balloons = []
score = 0
stepy = 2
game_over = False

def reset_balloon():
    return(randint(10, WIDTH - 10), randint(HEIGHT + 100, HEIGHT + 400))

for _ in range(10):
    balloon = Actor("balloon.png")
    balloon.center = reset_balloon()
    balloons.append(balloon)

def update():
    global score, stepy, game_over
    for balloon in balloons:
        balloon.y -= stepy
        balloon.x += randint(-1, 1)
        if balloon.bottom <= 0:
            balloon.center =  reset_balloon()
            score -= 1
    if score < 0:
        score = 0
        stepy = 0
        game_over = True

def draw():
    screen.blit(images.backdrop, (0, 0))
    for balloon in balloons:
        balloon.draw()
    screen.draw.text("Score: " + str(score), (10, 10))
    if game_over:
        screen.draw.textbox("Game Over", (120, 200, 400, 80), color = "red")

def on_mouse_down(pos):
    global score
    for balloon in balloons:
        if balloon.collidepoint(pos):
            balloon.center = reset_balloon()
            score += 1

pgzrun.go()