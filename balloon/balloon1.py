import pgzrun
from random import randint

WIDTH = 640
HEIGHT = 480
TITLE = "🎈 Ein roter Luftballon 🎈"
balloons = []

for _ in range(10):
    balloon = Actor("balloon.png")
    balloon.center = randint(10, WIDTH - 10), randint(HEIGHT + 100, HEIGHT + 400)
    balloons.append(balloon)

def update():
    for balloon in balloons:
        balloon.y -= 1
        balloon.x += randint(-1, 1)
        if balloon.bottom <= 0:
            balloon.center = randint(10, WIDTH - 10), randint(HEIGHT + 100, HEIGHT + 400)

def draw():
    # screen.fill((0, 80, 125))
    screen.blit(images.backdrop, (0, 0))
    for balloon in balloons:
        balloon.draw()

pgzrun.go()