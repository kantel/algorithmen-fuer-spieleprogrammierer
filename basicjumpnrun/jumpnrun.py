import pgzrun

WIDTH = 640
HEIGHT = 480
TITLE = "Jump'n'Run!"

ground_height = 400

player = Actor("player_idle")
player.bottomleft = (100, ground_height)

grounds = []
def make_ground():
    for i in range(10):
        ground = Actor("tile001")
        x = i*ground.width
        ground.topleft = (x, ground_height)
        grounds.append(ground)

def draw_ground():
    for ground in grounds:
        ground.draw()        

make_ground()

def update():
    pass

def draw():
    screen.fill((0, 200, 255))
    draw_ground()
    player.draw()

pgzrun.go()