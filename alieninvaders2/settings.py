class Settings(object):
    
    def __init__(self):
        self.WIDTH = 900
        self.HEIGHT = 600
        self.TITLE = "Alien Invaders"
        self.bg_color = (0, 80, 125)
        
        self.ship_speed_factor = 4.5
        self.laser_speed = -10
        
        self.number_of_enemies = 6
        
        self.FPS = 60