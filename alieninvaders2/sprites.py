import pygame as pg
from settings import Settings
import random
import os

file_path = os.path.dirname(os.path.abspath(__file__))
image_path = os.path.join(file_path, "images")

s = Settings()

class Player(pg.sprite.Sprite):
    
    def __init__(self):
        pg.sprite.Sprite.__init__(self)
        self.image = pg.image.load(os.path.join(image_path, "playerShip1_red.png")).convert_alpha()
        self.image = pg.transform.scale(self.image, (50, 38))
        self.rect = self.image.get_rect()
        self.rect.centerx = s.WIDTH/2
        self.rect.bottom = s.HEIGHT - 20
        self.center = float(self.rect.centerx)
        self.moving_right = False
        self.moving_left = False
        self.shooting = False
    
    def update(self):
        if self.moving_right and self.rect.right < s.WIDTH:
            self.center += s.ship_speed_factor
        if self.moving_left and self.rect.left > 0:
            self.center -= s.ship_speed_factor
        self.rect.centerx = self.center

class Enemy(pg.sprite.Sprite):
    
    def __init__(self):
        pg.sprite.Sprite.__init__(self)
        self.image = pg.image.load(os.path.join(image_path, "enemyRed1.png")).convert_alpha()
        self.image = pg.transform.scale(self.image, (47, 42))
        self.rect = self.image.get_rect()
        self.rect.x = random.randrange(s.WIDTH - self.rect.width)
        self.rect.y = random.randrange(-100, -40)
        self.speedy = random.randrange(2, 8)
        self.speedx = random.randrange(-2, 2)
    
    def update(self):
        self.rect.y += self.speedy
        self.rect.x += self.speedx
        if self.rect.top > (s.HEIGHT + 10 or self.rect.left < -25 or self.rect.right > s.WIDTH + 25):
            self.rect.x = random.randrange(s.WIDTH - self.rect.width)
            self.rect.y = random.randrange(-100, -40)
            self.speedy = random.randrange(2, 8)
            self.speedx = random.randrange(-2, 2)
    
class Laser(pg.sprite.Sprite):
    
    def __init__(self, x, y):
        pg.sprite.Sprite.__init__(self)
        self.image = pg.image.load(os.path.join(image_path, "laserRed16.png")).convert_alpha()
        # self.image = pg.transform.scale(self.image, (47, 42))
        self.rect = self.image.get_rect()
        self.rect.bottom = y
        self.rect.centerx = x
        self.speedy = s.laser_speed
    
    def update(self):
        self.rect.y += self.speedy
        if self.rect.bottom < 0:
            self.kill()
        