# Algorithmen für Spieleprogrammierer

In diesem Repositorium möchte ich Projekte zur Spieleprogrammierung sammeln, die ich mit Pygame, Pygame Zero und Processing.py erstellt habe. Zum einen sollen dabei einzelne Algorithmen zur Spieleprogrammierung vorgestellt werden, zum anderen möchte ich meine Kenntnisse in Pygame und Pygame Zero erweitern.

## Mit Pygame Zero

**Pygame Zero** ist eine freie (GPL) Bibliothek, die die Spieleprogrammierung mit Python besonders einfach machen will. Pygame Zero basiert auf Pygame.

![Pygame Bunt](pygamebunt.png)

## Links

- [Pygame Zero @ Read the Docs](https://pygame-zero.readthedocs.io/en/stable/)
- [Pygame Zero @ GitHub](https://github.com/lordmauve/pgzero)
- [Pygame Zero-Dokumentation als PDF](https://media.readthedocs.org/pdf/pygame-zero/latest/pygame-zero.pdf)
- [Pygame Zero @ PyPi](https://pypi.org/project/pgzero/)
