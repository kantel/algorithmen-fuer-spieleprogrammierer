import pgzrun

class Layer(Actor):
    
    def __init__(self, image):
        Actor.__init__(self, image)
        self.speed = 0
        self.turnvalue = 0
    
    def move(self):
        self.left -= self.speed
        if self.right <= 0:
            self.left = self.turnvalue
    
    def show(self):
        self.draw()

class Obstacle(Actor):
    
    def __init__(self, image, pos):
        Actor.__init__(self, image, pos)
        self.bottomleft = pos
        self.speed = 4

    def move(self):
        self.left -= self.speed
        if self.right <= 0:
            self.left = 3.4*WIDTH
    
    def show(self):
        self.draw()

class Player(Actor):
    
    def __init__(self, image, pos):
        Actor.__init__(self, image, pos)
        self.pos = pos
        self.status = "walking"
        self.hit = False
        self.count = 0
        self.vely = 0
        self.score = 0
        self.speed = 1
    
    def move(self):
        if self.status == "walking":
            self.count += self.speed
            if self.count > 10:
                self.count = 0
            if self.count < 6:
                self.image = "alienwalk1.png"
            else:
                self.image = "alienwalk2.png"
        if self.status == "jumping":
            self.image = "alienjump.png"
            self.bottom += self.vely
            self.vely += 0.1
            if self.bottom >= HEIGHT - 75:
                self.bottom = HEIGHT - 75
                self.vely = 0
                self.status = "walking"
                self.score += 1
                
    def show(self):
        self.draw()

WIDTH = 720
HEIGHT = 480
TITLE = "Jump, Älly, Jump!"

bg = Layer("sky_background")
bg.topleft = 0, 0
bg.turnvalue = 0

sun = Layer("sun")
sun.topleft = 500, 20
sun.turnvalue = 0

clouds = Layer("clouds_all")
clouds.topleft = WIDTH, 0
clouds.speed = 1
clouds.turnvalue = WIDTH

mountain1 = Layer("mountain_full")
mountain1.bottomleft = 0, HEIGHT - 75
mountain1.speed = 2
mountain1.turnvalue = 2*WIDTH

mountain2 = Layer("mountain_full")
mountain2.bottomleft = 1279, HEIGHT - 75
mountain2.speed = 2
mountain2.turnvalue = 2*WIDTH

ground1 = Layer("ground")
ground1.bottomleft = 0, HEIGHT + 10
ground1.speed = 4
ground1.turnvalue = WIDTH

ground2 = Layer("ground")
ground2.bottomleft = 1270, HEIGHT + 10
ground2.speed = 4
ground2.turnvalue = WIDTH

spikes1 = Obstacle("spikes", (WIDTH + 100, HEIGHT - 75))
cactus1 = Obstacle("cactus", (WIDTH + 600, HEIGHT - 75))
cactus2 = Obstacle("cactus", (WIDTH + 640, HEIGHT - 75))
cactus3 = Obstacle("cactus", (WIDTH + 670, HEIGHT - 75))
spikes2 = Obstacle("spikesbottomalt", (WIDTH + 1050, HEIGHT - 75))
spikes3 = Obstacle("spikesbottom", (WIDTH + 1150, HEIGHT - 75))
fence1  = Obstacle("fence", (WIDTH + 1620, HEIGHT - 75))
fence2  = Obstacle("fencebroken", (WIDTH + 1680, HEIGHT - 75))
fence3  = Obstacle("fence", (WIDTH + 1740, HEIGHT - 75))

layers = [bg, sun, clouds, mountain1, mountain2, ground1, ground2, spikes1, spikes2, spikes3,
          cactus1, cactus2, cactus3, fence1, fence2, fence3]

obstacles = [spikes1, cactus1, cactus2, cactus3, spikes2, spikes3, fence1, fence2, fence3]

alien = Player("alienwalk1", (50, HEIGHT - 115))

def on_key_down():
    global alien_status, alien_vely
    if keyboard[keys.SPACE] and alien.status == "walking":
       alien.status = "jumping"
       alien.vely += -5

def update():
    for layer in layers:
        layer.move()
    alien.move()
    for obstacle in obstacles:
        if alien.colliderect(obstacle):
            alien.score -= 1

def draw():
    for layer in layers:
        layer.show()
    alien.show()
    screen.draw.textbox("Score: " + str(alien.score), (0, 440, 100, 20))

pgzrun.go()