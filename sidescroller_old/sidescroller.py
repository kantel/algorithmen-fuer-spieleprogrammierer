import pgzrun

WIDTH = 720
HEIGHT = 480
TITLE = "Jump, Ally, Jump!"

############## Layer ############

# Hintergrundbild (Himmel - unbeweglich)
layer_bg = Actor("sky_background.png")
layer_bg.topleft = 0, 0
layer_bg.speed = 0

# Sonne (unbeweglich)
layer_sun = Actor("sun.png")
layer_sun.topleft = 500, 20
layer_sun.speed = 0

# Wolken
layer_clouds = Actor("clouds_all.png")
layer_clouds.topleft = WIDTH, 0
layer_clouds.speed = 1

# Berge
layer_mountain1 = Actor("mountain_full.png")
layer_mountain1.bottomleft = 0, HEIGHT - 75
layer_mountain1.speed = 2

layer_mountain2 = Actor("mountain_full.png")
layer_mountain2.bottomleft = 1279, HEIGHT - 75
layer_mountain2.speed = 2

# Grund
layer_ground1 = Actor("ground.png")
layer_ground1.bottomleft = 0, HEIGHT + 10
layer_ground1.speed = 4

layer_ground2 = Actor("ground.png")
layer_ground2.bottomleft = 1279, HEIGHT + 10
layer_ground2.speed = 4


layers = [layer_bg, layer_sun, layer_clouds, layer_mountain1, layer_mountain2]
grounds = [layer_ground1, layer_ground2]

########## Obstacles ###########

spikes = Actor("spikes.png", (WIDTH + 100, HEIGHT - 110))
spikes.speed = 4

cactus1 = Actor("cactus.png", (WIDTH + 450, HEIGHT - 110))
cactus1.speed = 4

cactus2 = Actor("cactus.png", (WIDTH + 490, HEIGHT - 110))
cactus2.speed = 4

cactus3 = Actor("cactus.png", (WIDTH + 530, HEIGHT - 110))
cactus3.speed = 4

spikes2 = Actor("spikesbottomalt.png", (WIDTH + 950, HEIGHT - 110))
spikes2.speed = 4

spikes3 = Actor("spikesbottom.png", (WIDTH + 1050, HEIGHT - 110))
spikes3.speed = 4

fence1 = Actor("fence.png", (WIDTH + 1590, HEIGHT - 110))
fence1.speed = 4

fence2 = Actor("fencebroken.png", (WIDTH + 1650, HEIGHT - 110))
fence2.speed = 4

fence3 = Actor("fence.png", (WIDTH + 1710, HEIGHT - 110))
fence3.speed = 4

obstacles = [spikes, cactus1, cactus2, cactus3, spikes2, spikes3, fence1, fence2, fence3]

########## Player ##########

alien = Actor("alienwalk1.png", (50, HEIGHT - 115))
# alien.bottomleft = 50, HEIGHT - 75
alien_status = "walking"
alien_count = 0
alien_vely = 0

########### Funktionen ########

def on_key_down():
    global alien_status, alien_vely
    if keyboard[keys.SPACE] and alien_status == "walking":
       alien_status = "jumping"
       alien_vely += -5

def alien_move():
    global alien_status, alien_count, alien_vely
    if alien_status == "walking":
        alien_count += 1
        if alien_count > 10:
            alien_count = 0
        if alien_count < 6:
            alien.image = "alienwalk1.png"
        else:
            alien.image = "alienwalk2.png"
    if alien_status == "jumping":
        alien.image = "alienjump.png"
        alien.bottom += alien_vely
        alien_vely += 0.1
        if alien.bottom >= HEIGHT - 75:
            alien.bottom = HEIGHT - 75
            alien_vely = 0
            alien_status = "walking"

########## Pygame Loop ##########

def update():
    for layer in layers:
        layer.left -= layer.speed
        if layer.right <= 0:
            layer.left = 2*WIDTH
    for ground in grounds:
        ground.left -= ground.speed
        if ground.right <= 0:
            ground.left = WIDTH
    for obstacle in obstacles:
        obstacle.left -= obstacle.speed
        if obstacle.right <= 0:
            obstacle.left = 3*WIDTH
    alien_move()

def draw():
    for layer in layers:
        layer.draw()
    for ground in grounds:
        ground.draw()
    for obstacle in obstacles:
        obstacle.draw()
    alien.draw()

pgzrun.go()