import pgzrun
from random import randint

WIDTH = 640
HEIGHT = 480
TITLE = "Fluffiger Flieger"

## Grund
ground1 = Actor("groundgrass")
gx1 = 0
ground1.bottomleft = (gx1, HEIGHT)
ground2 = Actor("groundgrass")
gx2 = WIDTH
ground2.bottomleft = (gx2, HEIGHT)
groundspeed = 1

## Felsen
rockgrass1 = Actor("rockgrass")
rockgrass1down = Actor ("rockgrassdown")
rx1 = 200
ry1 = randint(-100, 0)
rbottom = HEIGHT + ry1 + 50
if rbottom < HEIGHT:
    rbottom = HEIGHT
rockgrass1.bottomleft = (rx1, rbottom)
rockgrass1down.topleft = (rx1, ry1)

## Spieler
player = Actor("planeyellow1")
player.centerx = 100
player.centery = HEIGHT/2

def update():
    global rx1, ry1, rbottom, gx1, gx2
    rx1 -= groundspeed
    rockgrass1.bottomleft = (rx1, rbottom)
    rockgrass1down.topleft = (rx1, ry1)
    if rx1 <= -WIDTH:
        rx1 = WIDTH + 50
        ry1 = randint(-100, 0)
    rbottom = HEIGHT + ry1 + 50
    if rbottom < HEIGHT:
        rbottom = HEIGHT    
    gx1 -= groundspeed
    ground1.bottomleft = (gx1, HEIGHT)
    if gx1 <= -WIDTH:
        gx1 = WIDTH
    gx2 -= groundspeed
    ground2.bottomleft = (gx2, HEIGHT)
    if gx2 <= -WIDTH:
        gx2 = WIDTH

def draw():
    screen.blit(images.background, (0, 0))
    rockgrass1.draw()
    rockgrass1down.draw()
    ground1.draw()
    ground2.draw()
    player.draw()

pgzrun.go()