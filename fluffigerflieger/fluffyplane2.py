import pgzrun
from random import randint

WIDTH = 640
HEIGHT = 480
TITLE = "Fluffiger Flieger"

## Grund
ground1 = Actor("groundgrass")
gx1 = 0
ground1.bottomleft = (gx1, HEIGHT)
ground2 = Actor("groundgrass")
gx2 = WIDTH
ground2.bottomleft = (gx2, HEIGHT)
groundspeed = 1

## Felsen
rocks = []
rocksdown = []
for i in range(5):
    rock = Actor("rockgrass")
    rockdown = Actor("rockgrassdown")
    x = [200, 500, 800, 1100, 1300]
    rx = x[i]
    ry = randint(-100, 0)
    rock.bottom =  HEIGHT + ry + 50
    if rock.bottom < HEIGHT:
        rock.bottom = HEIGHT
    rock.bottomleft = (rx, rock.bottom)
    rocks.append(rock)
    rockdown.topleft = (rx, ry)
    rocksdown.append(rockdown)
## Spieler
player = Actor("planeyellow1")
player.centerx = 100
player.centery = HEIGHT/2

def update():
    global rx, ry, gx1, gx2
    for rock in rocks:
        rock.left -= groundspeed
        rock.bottomleft = (rock.left, rock.bottom)
        if rock.right <= -WIDTH:
            rock.left = WIDTH 
            ry = randint(-100, 0)
    for rock in rocksdown:
        rock.left -= groundspeed
        rock.topleft = (rock.left, ry)
        if rock.right <= -WIDTH:
            rock.left = WIDTH
            ry = randint(-100, 0)
            rbottom = HEIGHT + ry + 50
            if rbottom < HEIGHT:
                rbottom = HEIGHT
    gx1 -= groundspeed
    ground1.bottomleft = (gx1, HEIGHT)
    if gx1 <= -WIDTH:
        gx1 = WIDTH
    gx2 -= groundspeed
    ground2.bottomleft = (gx2, HEIGHT)
    if gx2 <= -WIDTH:
        gx2 = WIDTH

def draw():
    screen.blit(images.background, (0, 0))
    for rock in rocks:
        rock.draw()
    for rock in rocksdown:
        rock.draw()
    ground1.draw()
    ground2.draw()
    player.draw()

pgzrun.go()