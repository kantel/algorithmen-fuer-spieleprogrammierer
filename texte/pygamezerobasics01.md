![](images/balloon1.jpg)

## Spiele programmieren mit Pygame Zero -- die Basics (Teil 1)

Nachdem die [Python Arcade Library](http://cognitiones.kantel-chaos-team.de/multimedia/spieleprogrammierung/arcade.html) leider doch nicht meinen [Wünschen entsprach](http://blog.schockwellenreiter.de/2019/07/2019072001.html), habe ich beschlossen, mich wieder verstärkt auf [Pygame Zero](http://cognitiones.kantel-chaos-team.de/multimedia/spieleprogrammierung/pygamezero.html) zu konzentrieren und anhand eines Tutorials die Grundlagen dieser Bibliothek zu erkunden und Euch nahezubringen.

### Die Philosophie hinter Pygame Zero

Pygame Zero ist ein Wrapper über Pygame, der im Gegensatz zu Pygame ohne [Grundgerüst](http://blog.schockwellenreiter.de/2019/02/2019020402.html) auskommt, um ein leeres Fenster auf den Monitor zu zaubern. Es reicht eine leere Datei, zum Beispiel `empty.py`. Wenn diese dann in ihrem Verzeichnis im Terminal mit

~~~bash
pgzrun empty.py
~~~

aufgerufen wird, erscheint tatsächlich ein leeres, schwarzes Fenster in der Größe von 800 x 600 Pixeln und dem Titel *Pygame Zero Game* auf dem Bildschirm. Will man die Datei aus einem Editor oder `IDLE` heraus aufrufen, werden in `empty.py` zwei Zeilen benötigt:

~~~
import pgzrun

pgzrun.go()
~~~

Diese Datei kann dann entweder mit den Bordmitteln des Editors als Python-Skript gestartet oder in der Kommandozeile mit

~~~bash
python emtpy.py
~~~

aufgerufen werden.

Das Import-Statement muß immer am Anfang eines Pygame-Zero-Skriptes stehen und `pgzrun.go()` der letzte Befehl des Skriptes sein, alles was danach steht, wird vom Interpreter ignoriert.

Auch dieses Minimalprogramm erzeugt ein leeres, schwarzes Fenster mit dem Title *Pygame Zero Game* in der Größe von 800 x 600 Pixeln.

### Anpassungen

Will man nun Änderungen vornehmen, muß man in der Regel nur diese dem Skript mitteilen. So erzeugen die Zeilen

~~~python
import pgzrun

WIDTH = 320
HEIGHT = 240
TITLE = "🎈 Ein roter Luftballon 🎈"

def draw():
    screen.fill((0, 80, 125))

pgzrun.go()
~~~

ein 320 x 240 Pixel großes Fenster mit einem schönen Titel und einem meerblauen Hintergrund.

Dabei ist `draw()` eine Pygame-Zero-Methode, die ohne Parameter aufgerufen werden muß. Sie wird nur dann aktiv, wenn es tatsächlich etwas neues zu zeichnen gibt.

![](images/empty2.jpg)

Die Konstanten `WIDTH`, `HEIGHT` und `TITLE` werden von Pygame Zero verstanden und interpretiert.

### Ein kleines Programm

Mit diesem Vorwissen seid Ihr (fast) in der Lage, Euer erstes, kleines Pygame Zero Skript zu schreiben:

~~~python
import pgzrun
from random import randint

WIDTH = 640
HEIGHT = 480
TITLE = "🎈 Ein roter Luftballon 🎈"
balloons = []

for _ in range(10):
    balloon = Actor("balloon.png")
    balloon.center = randint(10, WIDTH - 10), randint(HEIGHT + 100, HEIGHT + 400)
    balloons.append(balloon)

def update():
    for balloon in balloons:
        balloon.y -= 1
        balloon.x += randint(-1, 1)
        if balloon.bottom <= 0:
            balloon.center = randint(10, WIDTH - 10), randint(HEIGHT + 100, HEIGHT + 400)

def draw():
    screen.blit(images.backdrop, (0, 0))
    for balloon in balloons:
        balloon.draw()

pgzrun.go()
~~~

Für alles, was sich auf dem Bildschirm bewegt oder was bei Kollisionen berücksichtigt werden soll, ist in Pygame Zero die Klasse `Actor` verantwortlich. Sie wird mit dem Namen eines Bildes, das sie repräsentieren soll, aufgerufen. Alle Bilder (und sonstigen *Assets* müssen in Pygame Zero in einem Verzeichnis `images` im Verzeichnis des Skriptes liegen, dann findet Pygame Zero sie automatisch.

Da das Hintergrundbild nicht als `Actor` fungieren muß, habe ich es mit

~~~python
    screen.blit(images.backdrop, (0, 0))
~~~

auf den Bildschrim gezaubert. Auch hier weiß Pygame Zero automatisch, wie mit `screen` umzugehen ist.

Aus Kompatibilitätsgründen gibt es ein paar Namensvorschriften für Bilder: Sie dürfen nicht mit einer Ziffer beginnen und dürfen nur aus Ziffern, dem Unterstrich (`_`) und **Kleinbuchstaben** bestehen.

Der `Actor` besitzt ein Menge an Eigenschaften und Methoden, die aus der `Rect`-Klasse von Pygame übernommen wurden. Interessierten wird ein [Blick in die Dokumentation](https://www.pygame.org/docs/ref/rect.html) empfohlen.

Neu ist ansonsten nur noch die `update()`-Methode. Auch sie gehört zu Pygame Zero und wird im Regelfalle sechzig Mal in der Sekunde aufgerufen. Dieses Intervall kann mit einem optionalen Parameter `dt` verändert werden.

So kann man mit gerade einmal 26 Zeilen Code schon etwas auf dem Monitor bewegen. Das ist das, was Pygame Zero »programmieren ohne *Boilerplate* nennt«.

### Credits

Das Bild des Luftballons habe ich den freien ([CC-BY 4.0](https://creativecommons.org/licenses/by/4.0/)) [Twemojis](https://twemoji.twitter.com/) von Twitter entnommen und das Hintergrundbild stammt aus dem ebenfalls freien ([CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/)), schier unerschöpflichen [Fundus von Kenney.nl](https://www.kenney.nl/assets/background-elements-redux).