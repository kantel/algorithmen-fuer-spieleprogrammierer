![](images/balloongameover.jpg)

## Spiele programmieren mit Pygame Zero – die Basics (Teil 2)

In diesem zweiten und abschließenden Teil meiner kleinen Einführung in [Pygame Zero](http://cognitiones.kantel-chaos-team.de/multimedia/spieleprogrammierung/pygamezero.html) (hier der [Link zu Teil 1](http://blog.schockwellenreiter.de/2019/07/2019072202.html)) möchte ich zeigen, wie man Interaktivität programmiert, wie man einen Punktestand anzeigen kann und wie man das Spiel beendet. Doch zuerst ein kleines Refaktoring:

In der ersten Version des Programms hatte ich zwei Mal die Zeile

~~~python
balloon.center = randint(10, WIDTH - 10), randint(HEIGHT + 100, HEIGHT + 400)
~~~

und das ist -- zumal diese Zeile in einer erweiterten Fassung des Programms durchaus noch häufiger auftauchen könnte -- leichter mit einer Funktion zu lösen. Also habe ich solch eine Funktion meinem Programm spendiert:

~~~python
def reset_balloon():
    return(randint(10, WIDTH - 10), randint(HEIGHT + 100, HEIGHT + 400))
~~~

Somit wird bei einem Reset jeder Ballon mit der Zeile

~~~python
balloon.center = reset_balloon()
~~~

auf eine zufällige Startposition zurückgesetzt.

Nun zum eigentlichen Spiel. Der Nutzer soll versuchen, jeden Ballon mit der Maus anzuklicken, bevor dieser endgültig am oberen Fensterrand verschwindet. Gelingt ihm dies, so bekommt er einen Punkt gutgeschrieben, doch für jeden Ballon, der entkommen kann, wird ihm ein Punkt abgezogen. Dafür habe ich zu Beginn des Programms eine Variable `score` deklariert und sie mit `0` initialisiert.

Pygame Zero kennt [viele vordeklarierte Funktionen](https://pygame-zero.readthedocs.io/en/stable/hooks.html#event-handling-hooks) zur Interaktion mit dem Nutzer, unter anderem auch `on_mouse_down(pos)`. Diese habe ich wie folgt benutzt:

~~~python
def on_mouse_down(pos):
    global score
    for balloon in balloons:
        if balloon.collidepoint(pos):
            balloon.center = reset_balloon()
            score += 1
~~~

`actor.collidepoint(pos)` gehört ebenfalls zu den vordefinierten Methoden von Pygame Zero und diese überprüft, ob das Tupel `pos` im `Rect` des `actor` liegt, in diesem Fall also, ob der Mauszeiger den Ballon getroffen hat. Ist dies der Fall, wird der Ballon neu initialisiert und der Punktestand um einen Punkt erhöht.

Für den anderen Fall, nämlich daß der Ballon entkommen konnte, mußte die `update()`-Methode geändert werden:

~~~python
def update():
    global score, stepy, game_over
    for balloon in balloons:
        balloon.y -= stepy
        balloon.x += randint(-1, 1)
        if balloon.bottom <= 0:
            balloon.center =  reset_balloon()
            score -= 1
    if score < 0:
        score = 0
        stepy = 0
        game_over = True
~~~

`stepy` ist die Geschwindigkeit, mit der sich der Ballon nach oben bewegt. Damit das Spiel nicht zu einfach wird, habe ich sie mit `2` initialisiert. `game_over` ist ein Flag, der gesetzt wird, wenn das Spiel verloren ist.

Jetzt noch die Texte: Pygame Zero besitzt eine [reichhaltige API zur Darstellung von Text](https://pygame-zero.readthedocs.io/en/stable/ptext.html), daraus habe ich die einfachste Form der `screen.draw.text(text, (x, y))`-Methode zur Anzeige der Punkte und die etwas elaboriertere `screen.draw.textbox(text, (x, y, w, h), color=col)`-Methode ausgewählt, um das Ende des Spiels anzuzeigen. Auch diese Text-Methoden besitzen sinnvolle Defaults, so daß man zum Beispiel die Farbe nur angeben muß, wenn man von diesen Defaults abweichen will.

Das alles gehört natürlich in `draw()`:

~~~python
def draw():
    screen.blit(images.backdrop, (0, 0))
    for balloon in balloons:
        balloon.draw()
    screen.draw.text("Score: " + str(score), (10, 10))
    if game_over:
        screen.draw.textbox("Game Over", (120, 200, 400, 80), color = "red")
~~~

Das war alles, damit habe ich in Pygame Zero mit knapp fünfzig Zeilen Quellcode ein vollständiges Spiel programmiert:

~~~python
import pgzrun
from random import randint

WIDTH = 640
HEIGHT = 480
TITLE = "🎈 Ein roter Luftballon 🎈"

balloons = []
score = 0
stepy = 2
game_over = False

def reset_balloon():
    return(randint(10, WIDTH - 10), randint(HEIGHT + 100, HEIGHT + 400))

for _ in range(10):
    balloon = Actor("balloon.png")
    balloon.center = reset_balloon()
    balloons.append(balloon)

def update():
    global score, stepy, game_over
    for balloon in balloons:
        balloon.y -= stepy
        balloon.x += randint(-1, 1)
        if balloon.bottom <= 0:
            balloon.center =  reset_balloon()
            score -= 1
    if score < 0:
        score = 0
        stepy = 0
        game_over = True

def draw():
    screen.blit(images.backdrop, (0, 0))
    for balloon in balloons:
        balloon.draw()
    screen.draw.text("Score: " + str(score), (10, 10))
    if game_over:
        screen.draw.textbox("Game Over", (120, 200, 400, 80), color = "red")

def on_mouse_down(pos):
    global score
    for balloon in balloons:
        if balloon.collidepoint(pos):
            balloon.center = reset_balloon()
            score += 1

pgzrun.go()
~~~

Mit der Maus stellt dieses Spiel keine Herausforderung dar, aber mit dem Trackpad meines betagten Powerbooks alle Ballons einzufangen, das ist schon eine Aufgabe.

Natürlich sind Erweiterungen möglich. So könnte man zum Beispiel nach dem Erreichen einer bestimmten Punktzahl die Geschwindigkeit (`stepy`) oder die Anzahl der Ballons erhöhen. Zufallgesteuert könnten auch Monsterballons mit schnellem Zick-Zack-Kurs auftauchen, deren Einfangen mit einer höheren Punktezahl belohnt wird.

All diese Erweiterungen überlasse ich der geneigten Leserin oder dem geneigten Leser. Für mich ist dieses Tutorial hiermit vorerst beendet, aber meine Erkundungen von Pygame Zero werde ich sicher fortsetzen.