import pygame as pg
from settings import Settings
from sprites import Player
import os

file_path = os.path.dirname(os.path.abspath(__file__))
image_path = os.path.join(file_path, "images")

s = Settings()

class Game(object):
    
    def __init__(self):
        pg.init()
        self.screen = pg.display.set_mode((s.WIDTH, s.HEIGHT))
        pg.display.set_caption(s.TITLE)
        self.background = pg.image.load(os.path.join(image_path, "background.gif")).convert()
        self.background_rect = self.background.get_rect()
        self.clock = pg.time.Clock()
        self.keep_going = True
    
    def new(self):
        self.all_sprites = pg.sprite.Group()
        self.player = Player()
        self.all_sprites.add(self.player)
        self.run()
    
    def run(self):
        self.playing = True
        self.clock.tick(s.FPS)
        while self.playing:
            self.watch_for_events()
            self.update()
            self.draw()
        
    def watch_for_events(self):
        for event in pg.event.get():
            if event.type == pg.QUIT or (event.type == pg.KEYDOWN and event.key == pg.K_ESCAPE):
                if self.playing:
                    self.playing = False
                self.keep_going = False
            elif event.type == pg.KEYDOWN:
                if event.key == pg.K_RIGHT:
                    self.player.moving_right = True
                elif event.key == pg.K_LEFT:
                    self.player.moving_left = True
            elif event.type == pg.KEYUP:
                if event.key == pg.K_RIGHT:
                    self.player.moving_right = False
                elif event.key == pg.K_LEFT:
                    self.player.moving_left = False
    
    def update(self):
        self.all_sprites.update()
    
    def draw(self):
        self.screen.fill(s.bg_color)
        self.screen.blit(self.background, self.background_rect)
        self.all_sprites.draw(self.screen)
        pg.display.flip()

g = Game()

while g.keep_going:
    g.new()
    g.update()

print("I did it, Babe!")
pg.quit()