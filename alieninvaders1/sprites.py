import pygame as pg
from settings import Settings
vec = pg.math.Vector2
import os

file_path = os.path.dirname(os.path.abspath(__file__))
image_path = os.path.join(file_path, "images")

s = Settings()

class Player(pg.sprite.Sprite):
    
    def __init__(self):
        pg.sprite.Sprite.__init__(self)
        self.image = pg.image.load(os.path.join(image_path, "playerShip1_red.png")).convert_alpha()
        self.image = pg.transform.scale(self.image, (50, 38))
        self.rect = self.image.get_rect()
        self.rect.centerx = s.WIDTH/2
        self.rect.bottom = s.HEIGHT - 20
        self.center = float(self.rect.centerx)
        self.moving_right = False
        self.moving_left = False
    
    def update(self):
        if self.moving_right and self.rect.right < s.WIDTH:
            self.center += s.ship_speed_factor
        if self.moving_left and self.rect.left > 0:
            self.center -= s.ship_speed_factor
        self.rect.centerx = self.center
