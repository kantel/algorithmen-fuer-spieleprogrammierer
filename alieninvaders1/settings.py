class Settings(object):
    
    def __init__(self):
        self.WIDTH = 900
        self.HEIGHT = 600
        self.TITLE = "Alien Invaders"
        self.bg_color = (0, 80, 125)
        
        self.ship_speed_factor = 2.5
        
        self.FPS = 60