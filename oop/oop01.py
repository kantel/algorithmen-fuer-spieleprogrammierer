import pgzrun

class Alien(Actor):
    
    def __init__(self, image, pos):
        Actor.__init__(self, image)
        self.pos = pos
        self.hit = False
    
    def show(self):
        if self.hit:
            screen.draw.textbox("Eek!", (100, 100, 200, 50))
        self.draw()
    
    def move(self):
        self.left += 2
        if self.left > WIDTH:
            self.right = 0
    
    def set_hurt(self):
        self.image = "alienhit"
        self.hit = True
        clock.schedule_unique(self.set_normal, 1.0)
    
    def set_normal(self):
        self.image = "alienwalk1"
        self.hit = False
        

WIDTH = 400
HEIGHT = 400
TITLE = "Älien OOP"

alien = Alien("alienwalk1", (200, 250))

def update():
    alien.move()

def draw():
    screen.fill((0, 80, 125))
    alien.show()

def on_mouse_down(pos):
    if alien.collidepoint(pos):
        alien.set_hurt()


pgzrun.go()